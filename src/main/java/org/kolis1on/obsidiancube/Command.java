package org.kolis1on.obsidiancube;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.kolis1on.obsidiancube.time.Timer;

public class Command implements CommandExecutor {

    public ObsidianCube plugin;
    public Command(ObsidianCube plugin){
        this.plugin = plugin;
    }
    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String s, String[] args) {
        if(sender instanceof Player){
            if(args.length == 1 && args[0].equals("reload") && sender.hasPermission(cmd.getPermission())){

                this.plugin.reloadConfig();
                this.plugin.saveConfig();

                Timer.loadDate();
                sender.sendMessage("Плагин успешно перезагружен");
            }

            if(args.length == 0){
                GenerateCube.spawnCube();
            }
        }
        return false;
    }
}
