package org.kolis1on.obsidiancube;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.world.World;
import org.bukkit.plugin.java.JavaPlugin;
import org.kolis1on.obsidiancube.time.Timer;

import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public final class ObsidianCube extends JavaPlugin {

    @Override
    public void onEnable() {



        File config = new File(getDataFolder() + File.separator + "config.yml");
        if (!config.exists()) {
            getConfig().options().copyDefaults(true);
            saveDefaultConfig();
        }
        new GenerateCube(this);
        new Timer(this);
        getCommand("cube").setExecutor(new Command(this));

        Timer.loadDate();
        Timer.startTime();
        Timer.miniTimer();


    }

    @Override
    public void onDisable() {
        getConfig().set("timer", Timer.timer);
    }

}
