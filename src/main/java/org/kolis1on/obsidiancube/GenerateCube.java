//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.kolis1on.obsidiancube;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.function.pattern.RandomPattern;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.block.BlockState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.kolis1on.obsidiancube.time.Timer;
import org.kolis1on.obsidiancube.utils.Region2Dim;

public class GenerateCube {
    public static ObsidianCube plugin;
    public int width;
    public int height;

    public GenerateCube(ObsidianCube plugin) {
        GenerateCube.plugin = plugin;
    }

    public static RandomLocation randomLocation() {
        return (new RandomLocation(Bukkit.getWorld("world_nether"))).withRegion(new Region2Dim(plugin.getConfig().getInt("border.x1"), plugin.getConfig().getInt("border.z1"), plugin.getConfig().getInt("border.x2"), plugin.getConfig().getInt("border.z2")));
    }

    public static void createCube(Location location) {
        Location newLocation = location.clone();
        newLocation.setX(newLocation.getX() + (double)width());
        newLocation.setY(newLocation.getY() + (double)height());
        newLocation.setZ(newLocation.getZ() + (double)width());
        World world = BukkitAdapter.adapt(Bukkit.getWorld("world_nether"));
        CuboidRegion selection = new CuboidRegion(world,
                BlockVector3.at(location.getX(), location.getY(), location.getZ()),
                BlockVector3.at(newLocation.getX(), newLocation.getY(), newLocation.getZ()));

        try {
            EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(world, -1);

            try {
                RandomPattern pat = new RandomPattern();
                BlockState obsidian = BukkitAdapter.adapt(Material.OBSIDIAN.createBlockData());
                BlockState cry_obsidian = BukkitAdapter.adapt(Material.CRYING_OBSIDIAN.createBlockData());
                BlockState lava = BukkitAdapter.adapt(Material.LAVA.createBlockData());
                pat.add(obsidian, plugin.getConfig().getDouble("chance.obsidian"));
                pat.add(cry_obsidian, plugin.getConfig().getDouble("chance.cry_obsidian"));
                pat.add(lava, plugin.getConfig().getDouble("chance.lava"));
                editSession.setBlocks(selection, pat);
                plugin.getConfig().set("location.world", location.getWorld().getName());
                plugin.getConfig().set("location.first.x", location.getX());
                plugin.getConfig().set("location.first.y", location.getY());
                plugin.getConfig().set("location.first.z", location.getZ());
                plugin.getConfig().set("location.second.x", newLocation.getX());
                plugin.getConfig().set("location.second.y", newLocation.getY());
                plugin.getConfig().set("location.second.z", newLocation.getZ());
                plugin.getConfig().set("timer", plugin.getConfig().getInt("cubeRemoveTime"));
                plugin.saveConfig();
                Timer.setTimer(plugin.getConfig().getInt("cubeRemoveTime"));
                String message = plugin.getConfig().getString("messages.spawnCube");
                message = message.replace("%x%", String.valueOf(selection.getCenter().getX())).replace(
                        "%y%", String.valueOf(selection.getCenter().getY())).replace(
                                "%z%", String.valueOf(selection.getCenter().getZ()));
                Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
                Timer.miniTimer();
            } catch (Throwable var10) {
                if (editSession != null) {
                    try {
                        editSession.close();
                    } catch (Throwable var9) {
                        var10.addSuppressed(var9);
                    }
                }

                throw var10;
            }

            if (editSession != null) {
                editSession.close();
            }
        } catch (MaxChangedBlocksException var11) {
            var11.printStackTrace();
        }

    }

    public static int getRandom(int min, int max) {
        int rn = (int)((double)min + Math.random() * (double)max);
        return rn;
    }

    public static int width() {
        return getRandom(plugin.getConfig().getInt("width.min"), plugin.getConfig().getInt("width.max"));
    }

    public static int height() {
        return getRandom(plugin.getConfig().getInt("height.min"), plugin.getConfig().getInt("height.max"));
    }

    public static void spawnCube() {
        randomLocation().findLocation().thenAccept((location) -> {
            createCube(location);
        });
    }
}
