package org.kolis1on.obsidiancube.time;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.world.World;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.kolis1on.obsidiancube.GenerateCube;
import org.kolis1on.obsidiancube.ObsidianCube;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class Timer {
    private static Map<String, String> dates = new HashMap<>();

    public static ObsidianCube plugin;
    public Timer(ObsidianCube plugin){
        this.plugin = plugin;
    }
    public static int timer = 0;
    public static void startTime(){
        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () ->{
            dates.forEach((day, time) ->{
                if(getDate().equals(day + " " + time)){
                    GenerateCube.spawnCube();
                }
            });
        }, 0, 20L * 60);

    }
    public static void miniTimer(){

        timer = plugin.getConfig().getInt("timer");
        if(timer > 0){
            new BukkitRunnable() {
                @Override
                public void run() {

                    timer--;
                    if(timer == 0) {

                        Bukkit.getScheduler().runTask(plugin, () -> {
                            Location location = new Location(Bukkit.getWorld("world_nether"),
                                    plugin.getConfig().getDouble("location.first.x"),
                                    plugin.getConfig().getDouble("location.first.y"),
                                    plugin.getConfig().getDouble("location.first.z"));

                            Location newLocation = new Location(Bukkit.getWorld("world_nether"),
                                    plugin.getConfig().getDouble("location.second.x"),
                                    plugin.getConfig().getDouble("location.second.y"),
                                    plugin.getConfig().getDouble("location.second.z"));

                            World world = BukkitAdapter.adapt(Bukkit.getWorld("world_nether"));
                            CuboidRegion region = new CuboidRegion(world, BlockVector3.at(location.getX(), location.getY(),location.getZ()),
                                    (BlockVector3.at(newLocation.getX(), newLocation.getY(),newLocation.getZ())));

                            EditSession session = WorldEdit.getInstance().getEditSessionFactory().getEditSession(region.getWorld(), -1);
                            Set<BlockVector2> chunks = region.getChunks();

                            world.regenerate(region, session);

                            session.flushSession();

                            plugin.getConfig().set("location.world", "world_nether");
                            plugin.getConfig().set("location.first.x", 0);
                            plugin.getConfig().set("location.first.y", 0);
                            plugin.getConfig().set("location.first.z", 0);

                            plugin.getConfig().set("location.second.x", 0);
                            plugin.getConfig().set("location.second.y", 0);
                            plugin.getConfig().set("location.second.z", 0);

                            plugin.getConfig().set("timer", 0);
                            plugin.saveConfig();
                            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.regenerateCube")));
                        });




                        cancel();
                    }
                }
            }.runTaskTimerAsynchronously(plugin, 0L, 20L);
        }
    }
    public static void loadDate(){
        dates.clear();
        String date = plugin.getConfig().getString("time");
        String[] dateSplit = date.split(" ");
        for(String d:dateSplit){
            String[] timeAndDay = d.split(",");
            String day = timeAndDay[0];
            String time = timeAndDay[1];

            dates.put(day, time);
        }
    }

    private static String getDate(){
        LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Moscow"));
        String dayNow = now.getDayOfWeek().toString();
        String timeNow;
        if(now.getMinute() < 10){
            timeNow = now.getHour() + ":0" + now.getMinute();
        }
        else {
            timeNow = now.getHour() + ":" + now.getMinute();
        }
        return dayNow + " " + timeNow;
    }
    public static void setTimer(int time){
        timer = time;
    }

}
